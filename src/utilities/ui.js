import MobileDetect from 'mobile-detect';

const DESKTOP_BREAKPOINT = 1024;

export const isMobileDevice = () => {
  const md = new MobileDetect(window.navigator.userAgent);
  return md.mobile() && md.phone();
};

// So sanh kích thước màn hình với một chiều dài định nghĩa
export const isMinScreenWidth = width => {
  const deviceWidth =
    window.innerWidth > 0 ? window.innerWidth : window.screen.width;
  return deviceWidth < width;
};

export const isMobileScreen = () => {
  const deviceWidth =
    window.innerWidth > 0 ? window.innerWidth : window.screen.width;
  return deviceWidth < DESKTOP_BREAKPOINT;
};

export const isDesktopScreen = () => {
  const deviceWidth =
    window.innerWidth > 0 ? window.innerWidth : window.screen.width;
  return deviceWidth >= DESKTOP_BREAKPOINT;
};

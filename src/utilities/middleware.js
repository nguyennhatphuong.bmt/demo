const localStorageMiddleware = store => next => action => {
  next(action);
};

export { localStorageMiddleware };

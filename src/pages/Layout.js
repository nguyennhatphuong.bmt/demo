import React, { Component } from 'react';

// Components
import { SideNav } from '../components';

// Style
import '../themes/default.scss';

export default class Layout extends Component {
  _generatePageClasses = () => '';

  _renderLeft = () => <SideNav />;

  _renderHeader = () => null;
  _renderBody = () => null;
  _renderFooter = () => null;

  render() {
    const { isOpen } = this.props;

    return (
      <div className={`e-container ${this._generatePageClasses()}`}>
        <div className="e-sidebar">{this._renderLeft()}</div>

        <div className="e-content">
          <div className="e-header">{this._renderHeader()}</div>

          <div className="e-body">{this._renderBody()}</div>

          <div className="e-footer">{this._renderFooter()}</div>
        </div>
      </div>
    );
  }
}

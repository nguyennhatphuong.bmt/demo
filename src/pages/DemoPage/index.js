import React from 'react';
import Layout from '../Layout';

// Utils
import { isMobileScreen } from '../../utilities/ui';

// Components
import { Image, Button, Input, Icon } from '../../components';

// Style
import './style.scss';

// Constants
// const MOBILE_SCREEN = isMobileScreen();

export default class DemoPage extends Layout {
  _Iframe = videoID => {
    return (
      <iframe
        className="iframe"
        title="Demo Video"
        width="560"
        height="315"
        src={`https://www.youtube.com/embed/${videoID}?rel=0&autoplay=0&mute=1`}
        frameBorder="0"
        allow="accelerometer; encrypted-media; gyroscope; picture-in-picture"
        allowFullScreen
      />
    );
  };

  _renderLeft = () => null;

  _renderHeader = () => {
    return (
      <div className="main-nav">
        <div className="left">
          <Button className="logo" iconName="home" />
          <p className="site-name">Funny Movies</p>
        </div>

        <div className="right">
          <Input id="email" type="text" placeholder="Email" />
          <Input id="password" type="password" placeholder="Password" />

          <Button
            className="primary-button"
            label="Login/Register"
            onClick={() => null}
          />
        </div>
      </div>
    );
  };

  _renderBody = () => {
    return (
      <div className="video-list">
        {/* Item */}
        <div className="video-item">
          <div className="video">{this._Iframe('ua3fq-yw6cY')}</div>

          <div className="video-info">
            <p className="video-title">Move title</p>

            <div className="meta-data">
              <div className="summary">
                <p className="shared-by">Shared by: {'demo@email.com'}</p>
                <p className="stats">
                  <Icon
                    className="stats-item like"
                    iconName="like"
                    label="89"
                  />

                  <Icon
                    className="stats-item dislike"
                    iconName="dislike"
                    label="12"
                  />
                </p>
              </div>

              <div className="actions">
                {/* iconName = like || like-active */}
                <Button className="like" iconName="like" />

                {/* iconName = dislike || dislike-active */}
                <Button className="dislike" iconName="dislike-active" />
              </div>
            </div>

            <div className="description">
              <p className="label">Description:</p>

              <p className="content">
                Animals are multicellular, eukaryotic organisms of the kingdom
                Animalia. The animal kingdom emerged as a basal clade within
                Apoikozoa as a sister of the choanoflagellates. Sponges are the
                most basal clade of animals. Animals are motile, meaning they
                can move spontaneously and independently at some point in their
                lives. Their body plan eventually becomes fixed as they develop,
                although some undergo a process of metamorphosis later in their
                lives. All animals are heterotrophs: they must ingest other
                organisms or their products for sustenance.
              </p>
            </div>
          </div>
        </div>

        {/* Item */}
        <div className="video-item">
          <div className="video">{this._Iframe('ua3fq-yw6cY')}</div>

          <div className="video-info">
            <p className="video-title">Move title</p>

            <div className="meta-data">
              <div className="summary">
                <p className="shared-by">Shared by: {'demo@email.com'}</p>
                <p className="stats">
                  <Icon
                    className="stats-item like"
                    iconName="like"
                    label="89"
                  />

                  <Icon
                    className="stats-item dislike"
                    iconName="dislike"
                    label="12"
                  />
                </p>
              </div>

              <div className="actions">
                {/* iconName = like || like-active */}
                <Button className="like" iconName="like" />

                {/* iconName = dislike || dislike-active */}
                <Button className="dislike" iconName="dislike-active" />
              </div>
            </div>

            <div className="description">
              <p className="label">Description:</p>

              <p className="content">
                Animals are multicellular, eukaryotic organisms of the kingdom
                Animalia. The animal kingdom emerged as a basal clade within
                Apoikozoa as a sister of the choanoflagellates. Sponges are the
                most basal clade of animals. Animals are motile, meaning they
                can move spontaneously and independently at some point in their
                lives. Their body plan eventually becomes fixed as they develop,
                although some undergo a process of metamorphosis later in their
                lives. All animals are heterotrophs: they must ingest other
                organisms or their products for sustenance.
              </p>
            </div>
          </div>
        </div>

        {/* Item */}
        <div className="video-item">
          <div className="video">{this._Iframe('ua3fq-yw6cY')}</div>

          <div className="video-info">
            <p className="video-title">Move title</p>

            <div className="meta-data">
              <div className="summary">
                <p className="shared-by">Shared by: {'demo@email.com'}</p>
                <p className="stats">
                  <Icon
                    className="stats-item like"
                    iconName="like"
                    label="89"
                  />

                  <Icon
                    className="stats-item dislike"
                    iconName="dislike"
                    label="12"
                  />
                </p>
              </div>

              <div className="actions">
                {/* iconName = like || like-active */}
                <Button className="like" iconName="like" />

                {/* iconName = dislike || dislike-active */}
                <Button className="dislike" iconName="dislike-active" />
              </div>
            </div>

            <div className="description">
              <p className="label">Description:</p>

              <p className="content">
                Animals are multicellular, eukaryotic organisms of the kingdom
                Animalia. The animal kingdom emerged as a basal clade within
                Apoikozoa as a sister of the choanoflagellates. Sponges are the
                most basal clade of animals. Animals are motile, meaning they
                can move spontaneously and independently at some point in their
                lives. Their body plan eventually becomes fixed as they develop,
                although some undergo a process of metamorphosis later in their
                lives. All animals are heterotrophs: they must ingest other
                organisms or their products for sustenance.
              </p>
            </div>
          </div>
        </div>
      </div>
    );
  };

  _generatePageClasses = () => 'demo-page';
}

/*
 * Desc: Fold 1
 * */

import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

// Utils
import { isMobileScreen } from '../../../utilities/ui';

// Component
import { Image, Button, Icon } from '../../../components';

// Style
import './style.scss';
import './mobile.scss';

// Constants
const MOBILE_SCREEN = isMobileScreen();

export default class Fold4 extends PureComponent {
  render() {
    return (
      <div className="fold four" id="contact">
        <div className="contact-info">
          <Image
            path={`coming-soon/fold-4/${
              MOBILE_SCREEN ? 'mobile-bg' : 'desktop-bg'
            }`}
            className="background"
          />

          <div className="detail">
            <p className="title">Contact us</p>

            <div className="info">
              <Icon
                iconName="address-contact-2"
                className="info-item"
                label="Lô 03, kdc An Cư 5, Phường Mân Thái, Quận Sơn Trà, TP Đà Nẵng"
              />

              <Icon
                iconName="phone-contact-2"
                className="info-item"
                label="0236.6522.833 / 0971989807"
              />

              <a
                href="mailto:info@seldemerhotelandsuites.com"
                className="email-contact-wrapper"
              >
                <Icon
                  iconName="email-contact-2"
                  className="info-item"
                  label="info@seldemerhotelandsuites.com"
                />
              </a>

              <div className="info-item social-connection">
                <a>
                  <Icon iconName="facebook-connect-2" label="Facebook" />
                </a>

                <a>
                  <Icon iconName="instagram-connect-2" label="Instagram" />
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

/*
 * Desc: Trang coming soon
 * */

import React from 'react';
import Layout from '../Layout';

// Utils
import { isMobileScreen } from '../../utilities/ui';

// Sub-container
import SideBar from './SideBar';
import Fold1 from './Fold1';
import Fold2 from './Fold2';
import Fold2Mobile from './Fold2/mobile';

import Fold3 from './Fold3';
import Fold3Mobile from './Fold3/mobile';

import Fold4 from './Fold4';

// Style
import './style.scss';
import './mobile.scss';

// Constants
const MOBILE_SCREEN = isMobileScreen();

export default class ComingSoonPage extends Layout {
  state = {
    activeMenu: '1',
    bottom: 0
  };

  componentDidMount() {
    window.addEventListener('scroll', this.handleScroll);
    const bottom = document.body.clientHeight;
    this.setState({ bottom });
  }

  componentWillUnmount() {
    window.removeEventListener('scroll', this.handleScroll);
  }

  handleScroll = e => {
    const bottom4 = document.getElementById('contact').getBoundingClientRect()
      .bottom;
    const height = document.getElementById('contact').getBoundingClientRect()
      .height;
    const { bottom } = this.state;
    const position1 = bottom - height * 0.8;
    const position2 = bottom - (height + height * 0.8);
    const position3 = bottom - (height * 2 + height * 0.8);
    const position4 = bottom - (height * 3 + height * 0.8);
    let activeMenu = '1';
    if (bottom4 > position1) activeMenu = '1';
    else if (bottom4 > position2) activeMenu = '2';
    else if (bottom4 > position3) activeMenu = '3';
    else if (bottom4 > position4) activeMenu = '4';
    this.setMenuActive(activeMenu);
  };

  setMenuActive = activeMenu => {
    this.setState({ activeMenu });
  };

  _renderHeader = () => {
    if (MOBILE_SCREEN) {
      return (
        <SideBar
          setMenuActive={this.setMenuActive}
          activeMenu={this.state.activeMenu}
        />
      );
    }

    return null;
  };

  _renderLeft = () => {
    if (!MOBILE_SCREEN) {
      return (
        <SideBar
          setMenuActive={this.setMenuActive}
          activeMenu={this.state.activeMenu}
        />
      );
    }

    return null;
  };

  _renderBody = () => {
    const { activeMenu } = this.state;
    return (
      <div className="body-container">
        <Fold1 activeMenu={activeMenu} />

        {MOBILE_SCREEN ? (
          <Fold2Mobile activeMenu={activeMenu} />
        ) : (
          <Fold2 activeMenu={activeMenu} />
        )}

        {MOBILE_SCREEN ? (
          <Fold3Mobile activeMenu={activeMenu} />
        ) : (
          <Fold3 activeMenu={activeMenu} />
        )}

        <Fold4 activeMenu={activeMenu} />
      </div>
    );
  };

  _renderFooter = () => null;

  _generatePageClasses = () => 'coming-soon-page';
}

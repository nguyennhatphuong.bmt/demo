/*
 * Desc: Fold 1
 * */

import React, { Component } from 'react';
import PropTypes from 'prop-types';

// Utils
import { isMobileScreen } from '../../../utilities/ui';

// Component
import { Image, Button } from '../../../components';

// Style
import './style.scss';
import './mobile.scss';

// Constants
const MOBILE_SCREEN = isMobileScreen();

export default class Fold1 extends Component {
  componentDidMount() {
    let video = document.getElementById('intro_video');
    let playBtn = document.getElementById('play_button');

    if (video) {
      // Theo Policy thì Chrome & Safari chặn auto play nếu có sound
      if (video.volume > 0) {
        video.volume = 0.0;
      }

      // Auto play
      if (video.paused) {
        if (video.muted || video.volume === 0) {
          video.play();
        }
      }

      // Tăng 10% volume mỗi 1s
      // let autoIncreaseVolume = setInterval(() => {
      //   if (video.volume < 0.9) {
      //     video.volume += 0.1;
      //   } else {
      //     clearInterval(autoIncreaseVolume);
      //   }
      // }, 1000);
      //
      // if (playBtn && !video.paused) {
      //   playBtn.setAttribute('style', 'display: none;');
      // }
    }

    // setInterval(() => {
    //   if (video.paused) {
    //     playBtn.setAttribute('style', 'display: initial;');
    //   } else {
    //     playBtn.setAttribute('style', 'display: none;');
    //   }
    // }, 1000);
  }

  _handlePlay = () => {
    let video = document.getElementById('intro_video');
    let playBtn = document.getElementById('play_button');

    if (video) {
      if (video.paused) {
        video.play();
        playBtn.setAttribute('style', 'display: none;');
      } else {
        video.pause();
        playBtn.setAttribute('style', 'display: initial;');
      }
    }
  };

  _autoIncreaseVolume = () => {
    let video = document.getElementById('intro_video');

    if (video && video.muted) {
      if (!video.paused) {
        // Nếu ko interactive với video, mà bỏ unmute thì ko play dc
        video.muted = false;

        // Tăng 10% volume mỗi 1s
        let autoIncreaseVolume = setInterval(() => {
          if (video.volume < 0.9) {
            video.volume += 0.1;
          } else {
            clearInterval(autoIncreaseVolume);
          }
        }, 1000);
      }
    }
  };

  render() {
    const videoID = 'FsrfzxNR56o';

    return (
      <div className="fold one" id="about">
        <div className="body">
          <div className="intro-video">
            {/* <video
              id="intro_video"
              className="intro"
              controls
              muted
              autoPlay
              // onMouseEnter={this._autoIncreaseVolume}
              >
              <track kind="captions" />
              <source
                src="/media/image/coming-soon/fold-1.mp4"
                type="video/mp4"
              />
            </video> */}

            <iframe
              className="intro-video"
              title="Sel De Mer - Hotel & Suit"
              width="560"
              height="315"
              src={`https://www.youtube.com/embed/${videoID}?rel=0&autoplay=1&mute=1`}
              frameBorder="0"
              allow="accelerometer; encrypted-media; gyroscope; picture-in-picture"
              allowFullScreen
            />

            {/* <Button
              iconName="play-video"
              className="play-video-button"
              id="play_button"
              onClick={this._handlePlay}
            /> */}
          </div>

          <p className="side-text">coming soon</p>
        </div>

        <div className="footer">
          <Button
            iconName="scroll-down-blue"
            label={
              MOBILE_SCREEN
                ? 'pull down to see more'
                : 'scroll down to see more'
            }
            className="scrolldown-button"
          />
        </div>
      </div>
    );
  }
}

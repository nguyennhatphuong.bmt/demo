/*
 * Desc: Fold 4
 * */

import React, { Component } from 'react';
import { API } from '../../../constants/serverConfig';

// Component
import { Image, Button, Input } from '../../../components';

// Style
import './style.scss';

export default class Fold3 extends Component {
  constructor(props) {
    super(props);

    this.state = {
      onLeft: false,
      onRight: false,
      messageLeft: null,
      messageRight: null
    };
  }

  componentDidMount() {
    document.addEventListener('mousedown', this.handleClickOutside);
    document.addEventListener('touchstart', this.handleClickOutside);
  }

  componentWillUnmount() {
    document.removeEventListener('mousedown', this.handleClickOutside);
    document.removeEventListener('touchstart', this.handleClickOutside);
  }

  handleClickOutside = event => {
    if (
      this.wrapperOutsideRef &&
      !this.wrapperOutsideRef.contains(event.target)
    ) {
      this._handleActive(null);
    }
  };

  _handleActive = position => {
    if (position === 'left') {
      this.setState({
        onLeft: true,
        onRight: false
      });
    } else if (position === 'right') {
      this.setState({
        onLeft: false,
        onRight: true
      });
    } else {
      this.setState({
        onLeft: false,
        onRight: false
      });
    }
  };

  _handleSendMail = async type => {
    let { emailTeam, emailPartner, messageTeam, messagePartner } = this.state;
    console.log(emailTeam, emailPartner, messageTeam, messagePartner);
    let email = '';
    let message = '';
    let re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

    if (type === 'TEAM') {
      email = emailTeam;
      message = messageTeam;
      if (!email || !re.test(email)) {
        this.setState({
          errorLeft: 'email is valid'
        });
        return false;
      }
      if (!message) {
        this.setState({
          errorRight: 'message is valid'
        });
        return false;
      }
    } else {
      email = emailPartner;
      message = messagePartner;
      if (!email || !re.test(email)) {
        this.setState({
          errorRight: 'email is valid'
        });
        return false;
      }

      if (!message) {
        this.setState({
          errorRight: 'message is valid'
        });
        return false;
      }
    }

    this.setState({
      messageLeft: '',
      messageRight: ''
    });

    const rawResponse = await fetch(API + '/mails', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({ email: email, type: type, message: message })
    });
    const content = await rawResponse.json();
    if (content.status === 'success') {
      console.log('success');
      let object = {};
      if (type === 'TEAM') {
        object = {
          messageLeft: 'Your message has been submitted!'
        };
      }
      if (type === 'PARTNER') {
        object = {
          messageRight: 'Your message has been submitted!'
        };
      }
      console.log(message);
      this.setState(object);
    } else {
      let object = {};
      if (type === 'TEAM') {
        object = {
          errorLeft: content.message
        };
      }
      if (type === 'PARTNER') {
        object = {
          errorRight: content.message
        };
      }
      this.setState(object);
    }
  };

  render() {
    const {
      onLeft,
      onRight,
      errorRight,
      errorLeft,
      messageLeft,
      messageRight
    } = this.state;
    console.log(messageLeft);
    let activeClass = '';
    if (onLeft) {
      activeClass = ' left-active';
    } else if (onRight) {
      activeClass = ' right-active';
    }

    return (
      <div
        className={`fold three desktop${activeClass}`}
        id="jobs_and_partnership"
        ref={r => {
          this.wrapperOutsideRef = r;
        }}
      >
        {/* LEFT SECTION */}
        <div className="section left">
          {onLeft ? (
            <div className="form">
              <Button
                label="Be our partner"
                className="corner-button"
                onClick={() => this._handleActive('right')}
              />

              {/* Left col */}
              <div className="left-col">
                <div className="title">
                  <p>Join</p>
                  <p>our</p>
                  <p>team</p>
                </div>
              </div>

              {/* Right col */}
              <div className="right-col">
                {/* Intro */}
                <div className="intro">
                  <div className="main-text">
                    <p>Contact us for more detailed</p>
                  </div>

                  <p className="sub-text">
                    Don&apos;t hestitate to contact, click here
                  </p>

                  <a href="mailto:hrm@seldemerhotelandsuites.com">
                    <Button label="Email us" />
                  </a>
                </div>

                {/* Form */}
                <div className="form-container">
                  <p className="text-guide">
                    Or leave your email and we will contact you back.
                  </p>

                  <div className="input-area">
                    <label htmlFor="email" className="text-label">
                      Your email
                    </label>
                    <input
                      type="text"
                      id="email"
                      className="text-field"
                      onChange={e =>
                        this.setState({
                          emailTeam: e.target.value
                        })
                      }
                    />
                  </div>

                  <div className="input-area">
                    <label className="text-label">Messages</label>
                    <div
                      contentEditable
                      className="text-field message"
                      onInput={e => {
                        this.setState({
                          messageTeam: e.target.innerText
                        });
                      }}
                    />
                  </div>
                  <div className="action-container">
                    <Button
                      label="Submit"
                      onClick={() => this._handleSendMail('TEAM')}
                    />
                    {errorLeft ? (
                      <span className="text-success"> {errorLeft} </span>
                    ) : null}
                    {messageLeft ? (
                      <span className="text-success"> {messageLeft} </span>
                    ) : null}
                  </div>
                </div>
              </div>
            </div>
          ) : (
            <div className="cover">
              <Image path="coming-soon/fold-3/mask-1" className="mask" />
              <div className="title" onClick={() => this._handleActive('left')}>
                <p>Join</p>
                <p>our</p>
                <p>team</p>
              </div>
            </div>
          )}
        </div>

        {/* RIGHT SECTION */}
        <div className="section right">
          {onRight ? (
            <div className="form">
              <Button
                label="Join our team"
                className="corner-button"
                onClick={() => this._handleActive('left')}
              />

              <div className="left-col">
                <div className="intro">
                  <div className="main-text">
                    <p>Contact us</p>
                    <p>for more detailed</p>
                  </div>
                </div>

                <div className="form-container">
                  <p className="text-guide">
                    Leave your email and we will contact you back.
                  </p>

                  <div className="input-area">
                    <label htmlFor="email" className="text-label">
                      Your email
                    </label>
                    <input
                      type="text"
                      id="email"
                      className="text-field"
                      onChange={e =>
                        this.setState({
                          emailPartner: e.target.value
                        })
                      }
                    />
                  </div>

                  <div className="input-area">
                    <label className="text-label">Messages</label>
                    <div
                      contentEditable
                      className="text-field message"
                      onInput={e => {
                        this.setState({
                          messagePartner: e.target.innerText
                        });
                      }}
                    />
                  </div>
                  <div>
                    <div className="action-container">
                      <Button
                        label="Submit"
                        onClick={() => this._handleSendMail('PARTNER')}
                      />
                      {errorRight ? (
                        <span className="text-success"> {errorRight} </span>
                      ) : null}
                      {messageRight ? (
                        <span className="text-success"> {messageRight} </span>
                      ) : null}
                    </div>
                  </div>
                </div>
              </div>

              <div className="right-col">
                <div className="title">
                  <p>Be</p>
                  <p>our</p>
                  <p>partner</p>
                </div>
              </div>
            </div>
          ) : (
            <div className="cover">
              <Image path="coming-soon/fold-3/mask-2" className="mask" />
              <div
                className="title"
                onClick={() => this._handleActive('right')}
              >
                <p>Be</p>
                <p>our</p>
                <p>partner</p>
              </div>
            </div>
          )}
        </div>
      </div>
    );
  }
}

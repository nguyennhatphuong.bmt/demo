/*
 * Desc: Fold 3
 * */

import React, { Component } from 'react';
import { API } from '../../../constants/serverConfig';

// Component
import { Image, Button, Input } from '../../../components';

// Style
import './mobile.scss';

export default class Fold3Mobile extends Component {
  constructor(props) {
    super(props);

    this.state = {
      onTop: false,
      onBottom: false,
      messageTop: null,
      messageBottom: null
    };
  }

  componentDidMount() {
    document.addEventListener('mousedown', this.handleClickOutside);
    document.addEventListener('touchstart', this.handleClickOutside);
  }

  componentWillUnmount() {
    document.removeEventListener('mousedown', this.handleClickOutside);
    document.removeEventListener('touchstart', this.handleClickOutside);
  }

  handleClickOutside = event => {
    if (
      this.wrapperOutsideRef &&
      !this.wrapperOutsideRef.contains(event.target)
    ) {
      this._handleActive('');
    }
  };

  _handleActive = position => {
    if (position === 'top') {
      this.setState({
        onTop: true,
        onBottom: false
      });
    } else if (position === 'bottom') {
      this.setState({
        onTop: false,
        onBottom: true
      });
    } else if (position === '') {
      this.setState({
        onTop: false,
        onBottom: false
      });
    }

    return null;
  };

  _handleSendMail = async type => {
    let { emailTeam, emailPartner, messageTeam, messagePartner } = this.state;
    console.log(emailTeam, emailPartner, messageTeam, messagePartner);
    let email = '';
    let message = '';
    let re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

    if (type === 'TEAM') {
      email = emailTeam;
      message = messageTeam;
      if (!email || !re.test(email)) {
        this.setState({
          errorLeft: 'email is valid'
        });
        return false;
      }
      if (!message) {
        this.setState({
          errorRight: 'message is valid'
        });
        return false;
      }
    } else {
      email = emailPartner;
      message = messagePartner;
      if (!email || !re.test(email)) {
        this.setState({
          errorRight: 'email is valid'
        });
        return false;
      }

      if (!message) {
        this.setState({
          errorRight: 'message is valid'
        });
        return false;
      }
    }

    this.setState({
      messageTop: '',
      messageBottom: ''
    });

    const rawResponse = await fetch(API + '/mails', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({ email: email, type: type, message: message })
    });
    const content = await rawResponse.json();
    if (content.status === 'success') {
      console.log('success');
      let object = {};
      if (type === 'TEAM') {
        object = {
          messageTop: 'Your message has been submitted!'
        };
      }
      if (type === 'PARTNER') {
        object = {
          messageBottom: 'Your message has been submitted!'
        };
      }
      console.log(message);
      this.setState(object);
    } else {
      let object = {};
      if (type === 'TEAM') {
        object = {
          errorLeft: content.message
        };
      }
      if (type === 'PARTNER') {
        object = {
          errorRight: content.message
        };
      }
      this.setState(object);
    }
  };

  render() {
    const {
      onTop,
      onBottom,
      errorRight,
      errorLeft,
      messageTop,
      messageBottom
    } = this.state;
    // console.log(messageTop);

    let activeClass = '';
    if (onTop) {
      activeClass = ' top-active';
    } else if (onBottom) {
      activeClass = ' bottom-active';
    }

    return (
      <div
        className={`fold three mobile${activeClass}`}
        id="jobs_and_partnership"
        ref={r => {
          this.wrapperOutsideRef = r;
        }}
      >
        {/* TOP SECTION */}
        <div
          className="section top"
          onClick={() => this._handleActive(onBottom ? 'top' : null)}
        >
          {onTop ? (
            <div className="form">
              <div className="title">
                <p>Join our team</p>
              </div>

              <p className="text-guide">
                Leave your email and we will contact you back.
              </p>

              <div className="input-area">
                <label htmlFor="email" className="text-label">
                  Your email
                </label>
                <input
                  type="text"
                  id="email"
                  className="text-field"
                  onChange={e =>
                    this.setState({
                      emailTeam: e.target.value
                    })
                  }
                />
              </div>

              <div className="input-area">
                <label className="text-label">Messages</label>
                <div
                  contentEditable
                  className="text-field message"
                  onInput={e => {
                    this.setState({
                      messageTeam: e.target.innerText
                    });
                  }}
                />
              </div>

              <div className="actions">
                <span className="response-message">
                  {errorLeft}
                  {messageTop}
                </span>

                <Button
                  label="Submit"
                  onClick={() => this._handleSendMail('TEAM')}
                />
              </div>
            </div>
          ) : (
            <div className="cover">
              {onBottom ? (
                <span
                  className="toogle-bar"
                  onClick={() => this._handleActive(onBottom ? 'top' : null)}
                />
              ) : (
                <div
                  className="cover-container"
                  onClick={() => this._handleActive('top')}
                >
                  <Image
                    path="coming-soon/fold-3/mobile/mask-1"
                    className="mask"
                  />

                  <div className="title">
                    <p>Join our team</p>
                  </div>
                </div>
              )}
            </div>
          )}
        </div>

        {/* BOTTOM SECTION */}
        <div
          className="section bottom"
          onClick={() => this._handleActive(onTop ? 'bottom' : null)}
        >
          {onBottom ? (
            <div className="form">
              <div className="title">
                <p>Be our partner</p>
              </div>

              <p className="text-guide">
                Leave your email and we will contact you back.
              </p>

              <div className="input-area">
                <label htmlFor="email" className="text-label">
                  Your email
                </label>
                <input
                  type="text"
                  id="email"
                  className="text-field"
                  onChange={e =>
                    this.setState({
                      emailPartner: e.target.value
                    })
                  }
                />
              </div>

              <div className="input-area">
                <label className="text-label">Messages</label>
                <div
                  contentEditable
                  className="text-field message"
                  onInput={e => {
                    this.setState({
                      messagePartner: e.target.innerText
                    });
                  }}
                />
              </div>

              <div className="actions">
                <span className="response-message">
                  {messageBottom}
                  {errorRight}
                </span>

                <Button
                  label="Submit"
                  onClick={() => this._handleSendMail('PARTNER')}
                />
              </div>
            </div>
          ) : (
            <div className="cover">
              {onTop ? (
                <span
                  className="toogle-bar"
                  onClick={() => this._handleActive(onTop ? 'bottom' : null)}
                />
              ) : (
                <div
                  className="cover-container"
                  onClick={() => this._handleActive('bottom')}
                >
                  <Image
                    path="coming-soon/fold-3/mobile/mask-2"
                    className="mask"
                  />

                  <div className="title">
                    <p>Be our partner</p>
                  </div>
                </div>
              )}
            </div>
          )}
        </div>
      </div>
    );
  }
}

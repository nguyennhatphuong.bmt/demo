/*
 * Desc: Fold 2
 * */

import React, { Component } from 'react';
import PropTypes from 'prop-types';

// Component
import { Image, Button } from '../../../components';

// Style
import './style.scss';

// Constants
const data = [
  {
    id: 1,
    cover: '/coming-soon/fold-2/rooms',
    description:
      'Sel de Mer - The story of little salt from the sea: silent but persistent, hidden yet everywhere; day by day, nurture, cultivate and add on the taste of life',
    label: '/coming-soon/fold-2/menu/1',
    default: true
  },
  {
    id: 2,
    cover: '/coming-soon/fold-2/spa-and-wellness',
    description:
      'Sel de Mer - The story of little salt from the sea: silent but persistent, hidden yet everywhere; day by day, nurture, cultivate and add on the taste of life',
    label: '/coming-soon/fold-2/menu/2'
  },
  {
    id: 3,
    cover: '/coming-soon/fold-2/meeting-and-events',
    description:
      'Sel de Mer - The story of little salt from the sea: silent but persistent, hidden yet everywhere; day by day, nurture, cultivate and add on the taste of life',
    label: '/coming-soon/fold-2/menu/3'
  },
  {
    id: 4,
    cover: '/coming-soon/fold-2/food-and-drink',
    description:
      'Sel de Mer - The story of little salt from the sea: silent but persistent, hidden yet everywhere; day by day, nurture, cultivate and add on the taste of life',
    label: '/coming-soon/fold-2/menu/4'
  }
];

export default class Fold2 extends Component {
  constructor(props) {
    super(props);

    this.state = {
      cover: '',
      description: '',
      activeId: 1
    };
  }

  componentWillMount() {
    return data.map(item => {
      if (item.default) {
        this.setState({
          cover: item.cover,
          description: item.description,
          activeId: item.id
        });
      }
    });
  }

  componentDidMount() {
    data.map(item => {
      if (item.default) {
        let ele = document.getElementById(`fold-2-${item.id}`);
        ele.classList.add('active');
      }
    });
  }

  _changeContext = (index, event = null) => {
    if (index) {
      return data.map(item => {
        if (item.id === index && event) {
          this.setState(
            {
              cover: item.cover,
              description: item.description,
              activeId: item.id
            },
            () => {
              // Tự động add và remove class animate sau 1s
              let imageCover = document.querySelectorAll(
                '#sneak_peek .menu-cover'
              );
              if (imageCover) {
                // Reset previous state
                imageCover[0].classList.remove('animate');
                imageCover[0].classList.add('animate');

                let autoAnimate = setTimeout(() => {
                  if (imageCover[0].classList.contains('animate')) {
                    imageCover[0].classList.remove('animate');
                    clearTimeout(autoAnimate);
                  } else {
                    console.log('bấm nhanh quá');

                    // imageCover[0].classList.add('animate');
                    // imageCover[0].classList.remove('animate');
                    clearTimeout(autoAnimate);
                  }
                }, 1000);
              }
            }
          );

          let current_ele = event.target;
          if (!current_ele.classList.contains('active')) {
            let all_ele = document.querySelectorAll('#sneak_peek .menu-item');
            for (let i = 0; i < all_ele.length; i++) {
              all_ele[i].classList.remove('active');
            }

            // Active menu item
            current_ele.classList.add('active');
          }
        }
      });
    }

    return null;
  };

  _renderMenuItem = activeId =>
    data.map(item => (
      <a
        key={item.id}
        className={`menu-item${item.id === activeId ? ' active' : ''}`}
        onClick={e => this._changeContext(item.id, e)}
      >
        <Image
          id={`fold-2-${item.id}`}
          // path={item.label + (item.id === activeId ? '-active' : '')}
          path={item.label}
          extension="png"
          className="menu-label"
        />
      </a>
    ));

  // Bỏ không xài
  _renderWhellNav = () => (
    <div
      id="divWheelnav"
      data-wheelnav
      data-wheelnav-init
      data-wheelnav-wheelradius="360"
      data-wheelnav-slicepath="WheelSlice"
      data-wheelnav-colors="#transparent"
      className="whell-nav"
    >
      <div data-wheelnav-navitemtext="Rooms" className="nav-item" />
      <div data-wheelnav-navitemtext="Spa & Wellness" className="nav-item" />
      <div data-wheelnav-navitemtext="Weddings & Events" className="nav-item" />
      <div data-wheelnav-navitemtext="Food & Drinks" className="nav-item" />
      <div data-wheelnav-navitemtext="" className="nav-item hidden" />
      <div data-wheelnav-navitemtext="" className="nav-item hidden" />
      <div data-wheelnav-navitemtext="" className="nav-item hidden" />
      <div data-wheelnav-navitemtext="" className="nav-item hidden" />
      <div data-wheelnav-navitemtext="" className="nav-item hidden" />
      <div data-wheelnav-navitemtext="" className="nav-item hidden" />
      <div data-wheelnav-navitemtext="" className="nav-item hidden" />
      <div data-wheelnav-navitemtext="" className="nav-item hidden" />
      <div data-wheelnav-navitemtext="" className="nav-item hidden" />
      <div data-wheelnav-navitemtext="" className="nav-item hidden" />
      <div data-wheelnav-navitemtext="" className="nav-item hidden" />
    </div>
  );

  render() {
    const { cover, description, activeId } = this.state;

    let activeClass = activeId ? ' active-' + activeId : '';

    return (
      <div className="fold two" id="sneak_peek">
        <div className="container">
          <div className="left">
            <Image path={cover} extension="png" className="menu-cover" />

            {/* {this._renderWhellNav()} */}

            <div className={`menu${activeClass}`}>
              {this._renderMenuItem(activeId)}
            </div>
          </div>

          <div className="right">
            <p className="title">Sneak Peek</p>
            <p className="description">{description}</p>
          </div>
        </div>

        {/* <p className="side-text">coming soon</p> */}
      </div>
    );
  }
}

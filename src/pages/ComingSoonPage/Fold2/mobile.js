/*
 * Desc: Fold 2
 * */

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Slider from 'react-slick';

// Utils
import { isMobileScreen } from '../../../utilities/ui';

// Component
import { Image, Button } from '../../../components';

// Style
import './mobile.scss';

// Constants
const MOBILE_SCREEN = isMobileScreen();
const data = [
  {
    id: 1,
    title: 'Rooms',
    cover: '/coming-soon/fold-2/rooms',
    description:
      'Sel de Mer - The story of little salt from the sea: silent but persistent, hidden yet everywhere; day by day, nurture, cultivate and add on the taste of life',
    label: '/coming-soon/fold-2/menu/1'
  },
  {
    id: 2,
    title: 'Spa & Wellness',
    cover: '/coming-soon/fold-2/spa-and-wellness',
    description:
      'Sel de Mer - The story of little salt from the sea: silent but persistent, hidden yet everywhere; day by day, nurture, cultivate and add on the taste of life',
    label: '/coming-soon/fold-2/menu/2'
  },
  {
    id: 3,
    title: 'Meeting & Events',
    cover: '/coming-soon/fold-2/meeting-and-events',
    description:
      'Sel de Mer - The story of little salt from the sea: silent but persistent, hidden yet everywhere; day by day, nurture, cultivate and add on the taste of life',
    label: '/coming-soon/fold-2/menu/3'
  },
  {
    id: 4,
    title: 'Food & Drink',
    cover: '/coming-soon/fold-2/food-and-drink',
    description:
      'Sel de Mer - The story of little salt from the sea: silent but persistent, hidden yet everywhere; day by day, nurture, cultivate and add on the taste of life',
    label: '/coming-soon/fold-2/menu/4'
  }
];

const settings = {
  dots: false,
  infinite: false,
  speed: 600,
  slidesToShow: 1,
  slidesToScroll: 1,
  centerMode: true,
  centerPadding: '74px',
  focusOnSelect: true
};

export default class Fold2Mobile extends Component {
  _renderSlideItem = () => {
    if (data) {
      return data.map(item => (
        <div className="slide-item" key={item.id}>
          <Image path={item.cover} extension="png" className="slide-cover" />

          <div className="slide-info">
            <p className="title">{item.title}</p>
            {/* <p className="description">{item.description}</p> */}
          </div>
        </div>
      ));
    }

    return null;
  };

  render() {
    return (
      <div className="fold two" id="sneak_peek">
        <div className="container">
          <Slider {...settings}>{this._renderSlideItem()}</Slider>

          <p className="description-text">
            Sel de Mer - The story of little salt from the sea: silent but
            persistent, hidden yet everywhere; day by day, nurture, cultivate
            and add on the taste of life
          </p>
        </div>

        <div className="footer">
          <p className="side-text">coming soon</p>
        </div>
      </div>
    );
  }
}

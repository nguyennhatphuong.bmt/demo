/*
 * Desc: Cột trái, xài riêng cho trang Coming Soon
 * */

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { HashLink as Link } from 'react-router-hash-link';

// Utils
import { isMobileScreen } from '../../../utilities/ui';

// Component
import { Image, Icon, Button } from '../../../components';

// Style
import './style.scss';
import './mobile.scss';

// Constants
const MOBILE_SCREEN = isMobileScreen();

export default class SideBar extends Component {
  constructor(props) {
    super(props);

    this.state = {
      hash: '',
      isExpanded: false
    };
  }

  componentDidMount() {
    let hash = window.location.hash;

    if (hash) {
      hash = hash.replace('#', '');

      this.setState({ hash: hash }, () => {
        let menuItem = document.querySelectorAll(
          `.side-bar #nav_${this.state.hash} .menu-label`
        );

        if (menuItem.length > 0) {
          menuItem[0].classList.add('active');
        }
      });
    }
  }

  _handleActive = (activeMenu, event) => {
    let menu = document.querySelectorAll('.side-bar .menu-item .menu-label');
    for (let i = 0; i < menu.length; i++) {
      menu[i].classList.remove('active');
    }

    if (event.target.className === 'menu-item') {
      event.target.querySelector('.menu-label').classList.add('active');
    } else {
      event.target.classList.add('active');
    }

    this.props.setMenuActive(activeMenu);
  };

  // DESKTOP ONLY
  _renderSideNav = () => {
    const rootPath = '/coming-soon';
    const { activeMenu } = this.props;

    return (
      <div className="side-bar">
        <div className="nav-header">
          <Link to={rootPath}>
            <Image className="e-logo" path="logo-color" />
          </Link>
        </div>

        <div className="nav-wrapper">
          <div className="nav-body">
            <Link
              className="menu-item"
              smooth
              to={`${rootPath}#about`}
              id="nav_about"
              onClick={e => this._handleActive('1', e)}
            >
              <p
                className={
                  activeMenu === '1' ? 'menu-label active' : 'menu-label'
                }
              >
                About
              </p>
            </Link>

            <Link
              className="menu-item"
              smooth
              to={`${rootPath}#sneak_peek`}
              id="nav_sneak_peek"
              onClick={e => this._handleActive('2', e)}
            >
              <p
                className={
                  activeMenu === '2' ? 'menu-label active' : 'menu-label'
                }
              >
                Sneak Peek
              </p>
            </Link>

            <Link
              className="menu-item"
              smooth
              to={`${rootPath}#jobs_and_partnership`}
              id="nav_jobs_and_partnership"
              onClick={e => this._handleActive('3', e)}
            >
              <p
                className={
                  activeMenu === '3' ? 'menu-label active' : 'menu-label'
                }
              >
                Jobs & Partnership
              </p>
            </Link>

            <Link
              className="menu-item"
              smooth
              to={`${rootPath}#contact`}
              id="nav_contact"
              onClick={e => this._handleActive('4', e)}
            >
              <p
                className={
                  activeMenu === '4' ? 'menu-label active' : 'menu-label'
                }
              >
                Contact
              </p>
            </Link>
          </div>
        </div>
      </div>
    );
  };

  // MOBILE ONLY
  _renderMobileNav = () => {
    const rootPath = '/coming-soon';
    const { activeMenu } = this.props;

    let { isExpanded } = this.state;

    return (
      <div className={`mobile-nav ${isExpanded ? 'expanded' : 'collapsed'}`}>
        {/* LOGO */}
        <div className="nav-header">
          <Button
            className="active-nav-button"
            iconName="burger"
            onClick={() => {
              this.setState({
                isExpanded: true
              });
            }}
          />

          <Link to={rootPath} className="logo-wrapper">
            <Image className="e-logo" path="logo-color" />
          </Link>
        </div>

        {/* SIDE NAV */}
        <div className="nav-content">
          <Button
            className="active-nav-button"
            iconName="close-nav"
            onClick={() => {
              this.setState({
                isExpanded: false
              });
            }}
          />

          <div className="nav-body">
            <Link
              className="menu-item"
              smooth
              to={`${rootPath}#about`}
              id="nav_about"
              onClick={e => this._handleActive('1', e)}
            >
              <p
                className={
                  activeMenu === '1' ? 'menu-label active' : 'menu-label'
                }
              >
                About
              </p>
            </Link>

            <Link
              className="menu-item"
              smooth
              to={`${rootPath}#sneak_peek`}
              id="nav_sneak_peek"
              onClick={e => this._handleActive('2', e)}
            >
              <p
                className={
                  activeMenu === '2' ? 'menu-label active' : 'menu-label'
                }
              >
                Sneak Peek
              </p>
            </Link>

            <Link
              className="menu-item"
              smooth
              to={`${rootPath}#jobs_and_partnership`}
              id="nav_jobs_and_partnership"
              onClick={e => this._handleActive('3', e)}
            >
              <p
                className={
                  activeMenu === '3' ? 'menu-label active' : 'menu-label'
                }
              >
                Jobs & Partnership
              </p>
            </Link>

            <Link
              className="menu-item"
              smooth
              to={`${rootPath}#contact`}
              id="nav_contact"
              onClick={e => this._handleActive('4', e)}
            >
              <p
                className={
                  activeMenu === '4' ? 'menu-label active' : 'menu-label'
                }
              >
                Contact
              </p>
            </Link>
          </div>
        </div>
      </div>
    );
  };

  render() {
    if (MOBILE_SCREEN) {
      return this._renderMobileNav();
    }

    return this._renderSideNav();
  }
}

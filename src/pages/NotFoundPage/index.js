import React, { Component } from 'react';

// Components
import { Image, Button } from '../../components';

// Style
import './style.scss';

export default class NotFoundPage extends Component {
  render() {
    return (
      <div className="not-found-page">
        <div className="content">
          <Image path="logo-color" />

          <p className="message">Trang này không tồn tại.</p>
          <Button
            className="primary-button"
            label="Quay lại trang chủ"
            iconName="arrow"
            onClick={() => this.props.history.push('/')}
          />
        </div>
      </div>
    );
  }
}

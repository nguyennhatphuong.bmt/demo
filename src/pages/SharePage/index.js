import React from 'react';
import Layout from '../Layout';

// Utils
import { isMobileScreen } from '../../utilities/ui';

// Components
import { Image, Button, Input } from '../../components';

// Style
import './style.scss';

// Constants
// const MOBILE_SCREEN = isMobileScreen();

export default class SharePage extends Layout {
  _renderLeft = () => null;

  _renderHeader = () => {
    return (
      <div className="main-nav">
        <div className="left">
          <Button className="logo" iconName="home" />
          <p className="site-name">Funny Movies</p>
        </div>

        <div className="right">
          <p className="user-name">Welcome {'user@mail.com'}</p>

          <Button
            className="primary-button"
            label="Share a movie"
            onClick={() => null}
          />

          <Button
            className="secondary-button"
            label="Logout"
            onClick={() => null}
          />
        </div>
      </div>
    );
  };

  _renderBody = () => {
    return (
      <div className="share-form">
        <p className="form-title">Share a Youtube movie</p>

        <Input
          id="video_url"
          type="text"
          label="Youtube URL:"
          placeholder="Video url"
        />

        <Button
          className="primary-button share-button"
          label="Share"
          onClick={() => null}
        />
      </div>
    );
  };

  _generatePageClasses = () => 'demo-page share';
}

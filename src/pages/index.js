import ComingSoonPage from './ComingSoonPage';

import DemoPage from './DemoPage';
import SharePage from './SharePage';
import NotFoundPage from './NotFoundPage';

export { ComingSoonPage, DemoPage, SharePage, NotFoundPage };

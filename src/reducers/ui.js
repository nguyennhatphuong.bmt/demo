import { CHANGE_DIMENSION } from '../constants/action-types';

const DESKTOP_BREAKPOINT = 1280;

const deviceWidth =
  window.innerWidth > 0 ? window.innerWidth : window.screen.width;

const initialState = {
  isMobileScreen: deviceWidth < DESKTOP_BREAKPOINT,
};

export default (state = initialState, action) => {
  switch (action.type) {
    case CHANGE_DIMENSION:
      return { isMobileScreen: action.payload };

    default:
      return state;
  }
};

import superagentPromise from 'superagent-promise';
import _superagent from 'superagent';
import uuidv4 from 'uuid/v4';

const localStorageKey = 'redux-store-tab-sync';

function isPromise(v) {
  return v && typeof v.then === 'function';
}

const superagent = superagentPromise(_superagent, global.Promise);

const tokenPlugin = req => {
  const token = window.localStorage.getItem('jwt');
  if (token) {
    req.set('authorization', `Bearer ${token}`);
  }
};

const promiseMiddleware = ({ dispatch }) => next => action => {
  // To create this middleware we used the time() and timeEnd() console methods
  // that record a benchmark with the name provided as a string.
  // We start the timing before running an action, using the action.type as a name.
  // Then, we tell the browser to print the timing after the action is done.
  // This way we can potentially catch poorly performing reducer implementations.
  // console.time(action.type);
  if (isPromise(action.payload)) {
    action.payload.then(
      res => {
        if (typeof res === 'undefined') {
          return;
        }
        let tempAction = action;
        tempAction.payload = res;
        dispatch(tempAction);
      },
      err => {
        // browserHistory.push('/500');
        // history.push('/500');
        let tempAction = action;
        tempAction.payload = err;
        dispatch(action);
      }
    );
  } else {
    next(action);
  }
  // console.timeEnd(action.type);
};

const APIMiddleware = ({ dispatch }) => next => action => {
  if (action.type !== 'API') {
    return next(action);
  }
  const { payload } = action;

  return superagent
    .get(payload.url)
    .use(tokenPlugin)
    .then(
      function onSuccess(res) {
        dispatch({ type: payload.SUCCESS, payload: res.body.result });
      },
      function onError(err) {
        console.log(err);
        dispatch({ type: payload.ERROR, payload: 'something happen bad' });
      }
    );
};

const getCurrentTabID = () => {
  let currentTabID = sessionStorage.getItem('tabID');
  if (!currentTabID) {
    let tabID = uuidv4();
    sessionStorage.setItem('tabID', tabID);
    return tabID;
  }
  return currentTabID;
};

const indentifyAction = action => {
  return {
    tabID: getCurrentTabID(),
    action
    // source: 'OTHER_TAB'
  };
};

const localStorageMiddleware = () => next => indentifiedAction => {
  if (!indentifiedAction.tabID) {
    // no tabID, mean new action dispatch from current browser
    let action = indentifiedAction;

    // NOTE: có thể check 1 số action đặc biệt ở đây
    switch (action.type) {
      // mean other action, ex: login, fetch topic ...
      default:
        return next(action);
    }
  } else {
    // got tabID, action load from localStorage & from other browser tab
    if (indentifiedAction.tabID !== getCurrentTabID()) {
      return next(indentifiedAction.action);
    }

    // action load from localStorage & from same browser tab
    localStorage.removeItem(localStorageKey);
    return null;
  }
};

const localStorageListener = store => {
  return () => {
    const indentifiedAction = JSON.parse(localStorage.getItem(localStorageKey));

    if (
      indentifiedAction &&
      indentifiedAction.action &&
      indentifiedAction.action.type
    ) {
      if (indentifiedAction.tabID !== getCurrentTabID()) {
        store.dispatch(indentifiedAction);
        localStorage.removeItem(localStorageKey);
      }
    }
  };
};

export {
  promiseMiddleware,
  localStorageMiddleware,
  localStorageListener,
  APIMiddleware
};

import { CHANGE_DIMENSION } from '../constants/action-types';

const changeDimension = (isMobileScreen = false) => dispatch => {
  dispatch({
    type: CHANGE_DIMENSION,
    payload: isMobileScreen,
  });
};

export { changeDimension };

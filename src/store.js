import thunk from 'redux-thunk';
import { applyMiddleware, createStore } from 'redux';
import { createLogger } from 'redux-logger';
import { composeWithDevTools } from 'redux-devtools-extension/developmentOnly';
import { persistStore, persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage';
import {
  promiseMiddleware,
  localStorageMiddleware,
  APIMiddleware,
  localStorageListener
} from './middleware';
import reducers from './reducers';

const getMiddleware = () => {
  if (process.env.BUILD === 'prod') {
    return applyMiddleware(thunk, promiseMiddleware, localStorageMiddleware);
  }

  // Enable additional logging in non-production environments.
  if (process.env.REACT_APP_SERVER_CONFIG !== 'production') {
    return applyMiddleware(
      thunk,
      promiseMiddleware,
      createLogger(),
      APIMiddleware,
      localStorageMiddleware
    );
  }

  return applyMiddleware(
    thunk,
    promiseMiddleware,
    APIMiddleware,
    localStorageMiddleware
  );
};

const persistConfig = {
  key: 'root',
  storage,
  whitelist: ['user']
};

const persistedReducer = persistReducer(persistConfig, reducers);

export default () => {
  let store = createStore(
    persistedReducer,
    composeWithDevTools(getMiddleware())
  );
  let persistor = persistStore(store);

  window.addEventListener('storage', localStorageListener(store));

  return { store, persistor };
};

import React, { PureComponent } from 'react';
import uuidv4 from 'uuid/v4';

// Components
import { Icon, Image } from '../../index';

// Style
import './style.scss';

export default class SideNav extends PureComponent {
  _renderMenu = () => {
    const data = [
      { label: 'Home', href: '/' },
      { label: 'Rooms & Suites', href: '/rooms' },
      { label: 'Restaurant & Bar', href: '/restaurant-and-bar' },
      { label: 'Spa & Wellness', href: '/spa-and-wellness' },
      { label: 'Meetings & Events', href: '/meetings-and-events' },
      { label: 'Special Offer', href: '/special-offer' },
      { label: 'Our story', href: '/our-story' },
      { label: 'Gallery', href: '/' }
    ];
    let menuContent = [];
    data.map(item => {
      menuContent.push(
        <a className="menu-item" href={`${item.href}`} key={uuidv4()}>
          <p className="menu-label">{item.label}</p>
        </a>
      );
    });

    return menuContent;
  };

  render() {
    const { inverted } = this.props;

    return (
      <div className={`side-nav${inverted ? ' inverted' : ''}`}>
        <div className="nav-header">
          <a href="/">
            <Image className="e-logo" path={inverted ? 'logo-color' : 'logo'} />
          </a>
        </div>

        <div className="nav-wrapper">
          <div className="nav-body">{this._renderMenu()}</div>

          <div className="nav-footer">
            <div className="social-links">
              <a>
                <Icon iconName="youtube-connect-2" />
              </a>

              <a href="https://www.linkedin.com/in/sel-de-mer-hotel-and-suites-da-nang-8b6287181/">
                <Icon iconName="linkedin-connect" />
              </a>

              <a href="https://www.facebook.com/seldemerhotelandsuites.official/">
                <Icon iconName="facebook-connect" />
              </a>

              <a href="https://www.instagram.com/seldemerdanang/">
                <Icon iconName="instagram-connect" />
              </a>
            </div>

            {/* <div className="site-languages">
              <a className="language-option active">EN</a>
              <a className="language-option">VI</a>
            </div> */}
          </div>
        </div>
      </div>
    );
  }
}

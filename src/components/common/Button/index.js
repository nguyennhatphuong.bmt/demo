import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

// Components
import { Icon } from '../../index';

// Style
import './style.scss';

export default class Button extends PureComponent {
  static propTypes = {
    className: PropTypes.string,
    iconName: PropTypes.string,
    label: PropTypes.string,
    onClick: PropTypes.func,
  };

  static defaultProps = {
    className: null,
    iconName: null,
    label: null,
    onClick: () => null,
  };

  render() {
    const { className, label, iconName, children, onClick, id } = this.props;

    return (
      <button
        className={`button${className ? ' ' + className : ''}`}
        onClick={onClick}
        id={id}
      >
        {iconName ? <Icon iconName={iconName} /> : null}
        {label ? <span className="button-label">{label}</span> : children}
      </button>
    );
  }
}

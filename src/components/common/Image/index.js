/*
 * Desc: render ảnh theo path hoặc src
 * */

import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

// Styles
import './style.scss';

export default class Image extends PureComponent {
  static propTypes = {
    src: PropTypes.string,
    path: PropTypes.string,
    extension: PropTypes.string,
    className: PropTypes.string,
    alt: PropTypes.string,
    style: PropTypes.object,
    fillMode: PropTypes.string
  };

  static defaultProps = {
    src: null,
    path: null,
    extension: 'svg',
    className: '',
    alt: '',
    style: {},
    fillMode: ''
  };

  _renderImage = () => {
    const { src, path, extension, className, alt, style, id } = this.props;
    const DEFAULT_IMG = '/media/image/default/not-found.svg';

    let { fillMode } = this.props;
    let modeList = ['fill', 'contain', 'cover', 'none', 'scale-down'];

    if (modeList.indexOf(fillMode) > -1) {
      fillMode = ' obj-' + fillMode;
    } else {
      fillMode = '';
    }

    if (!src && !path) {
      return null;
    }
    return (
      <img
        id={id}
        className={`image${fillMode}${className ? ' ' + className : ''}`}
        src={src || `/media/image/${path}.${extension}`}
        onError={e => {
          e.target.src = DEFAULT_IMG;
        }}
        style={style}
        alt={alt || 'media-object'}
      />
    );
  };

  render() {
    return this._renderImage();
  }
}

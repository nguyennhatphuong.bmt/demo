/*
 * Desc: Input
 * */

import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

// Component
import { Icon } from '../../index';

// Styles
import './style.scss';

export default class Input extends PureComponent {
  static propTypes = {
    label: PropTypes.string,
    id: PropTypes.string,
    className: PropTypes.string,
    type: PropTypes.string,
    name: PropTypes.string,
    value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    placeholder: PropTypes.string,
    style: PropTypes.object,
    accept: PropTypes.string,
    maxLength: PropTypes.number,
    pattern: PropTypes.string,
    required: PropTypes.bool,
    checked: PropTypes.bool,
    disabled: PropTypes.bool,
    autoComplete: PropTypes.bool,
    readOnly: PropTypes.bool,
    alt: PropTypes.string,
    ref: PropTypes.oneOfType([PropTypes.string, PropTypes.func]),
    onChange: PropTypes.func,
    onClick: PropTypes.func,
    onKeyUp: PropTypes.func
  };

  static defaultProps = {
    label: null,
    id: null,
    className: '',
    type: null,
    name: null,
    value: undefined,
    placeholder: null,
    style: {},
    accept: null,
    maxLength: null,
    pattern: null,
    required: false,
    checked: false,
    disabled: false,
    autoComplete: null,
    readOnly: null,
    alt: null,
    ref: () => null,
    onChange: () => null,
    onClick: () => null,
    onKeyUp: () => null
  };

  _renderInput = () => {
    const {
      label,
      id,
      name,
      value,
      placeholder,
      style,
      maxLength,
      pattern,
      required,
      checked,
      disabled,
      autoComplete,
      readOnly,
      ref,
      onChange,
      onClick,
      onKeyUp
    } = this.props;

    let { className, type, accept, alt, iconName } = this.props;

    // Đúng type thì mới return input tương ứng
    let types = [
      'button',
      'checkbox',
      'color',
      'date',
      'datetime-local',
      'email',
      'file',
      'hidden',
      'image',
      'month',
      'number',
      'password',
      'radio',
      'range',
      'reset',
      'search',
      'submit',
      'tel',
      'text',
      'time',
      'url',
      'week'
    ];
    if (types.indexOf(type) <= -1) {
      return null;
    }

    // Chỉ với 'type' tương ứng mới sử dụng được 1 số props đặc biệt
    switch (type) {
      case 'image':
        if (!alt) {
          alt = 'media-object';
        }
        accept = null;
        break;
      case 'file':
        alt = null;
        break;
      default:
        accept = null;
        alt = null;
    }

    let input = (
      <input
        id={id}
        className={`input-field${className ? ' ' + className : ''}`}
        type={type}
        name={name}
        value={value}
        placeholder={placeholder}
        style={style}
        maxLength={maxLength}
        pattern={pattern}
        required={required}
        checked={checked}
        accept={accept}
        disabled={disabled}
        autoComplete={autoComplete}
        readOnly={readOnly}
        alt={alt}
        ref={ref}
        onChange={onChange}
        onClick={onClick}
        onKeyUp={onKeyUp}
      />
    );

    if (iconName) {
      return (
        <div className="input-field-wrapper">
          {input}
          <Icon iconName={iconName} className="input-icon" />
        </div>
      );
    }

    return input;
  };

  render() {
    const { label, id } = this.props;
    let { className } = this.props;

    if (label) {
      return (
        <div className={`input${className ? ' ' + className : ''}`}>
          <label htmlFor={id} className="input-label">
            {label}
          </label>
          {this._renderInput()}
        </div>
      );
    }

    return this._renderInput();
  }
}

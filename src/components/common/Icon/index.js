/*
 * Desc: Với "iconName" tương ứng sẽ render ra icon tương ứng
 * */

import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

// Styles
import './style.scss';

export default class Icon extends PureComponent {
  static propTypes = {
    iconName: PropTypes.string.isRequired,
    className: PropTypes.string,
    label: PropTypes.string
  };

  static defaultProps = {
    // iconName: '',
    className: '',
    label: ''
  };

  _renderIcon = () => {
    if (this.props.className || this.props.label) {
      return (
        <span className={`icon-wrapper ${this.props.className}`}>
          <i className={`icon ${this.props.iconName}-icon`} />

          {this.props.label ? (
            <span className="icon-label">{this.props.label}</span>
          ) : null}
        </span>
      );
    }

    return <i className={`icon ${this.props.iconName}-icon`} />;
  };

  render() {
    return this._renderIcon();
  }
}

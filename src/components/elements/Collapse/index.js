/*
 * Desc: Collapse Box
 * */

import React, { Component } from 'react';
import PropTypes from 'prop-types';

// Components
import { Button } from '../../index';

// Style
import './style.scss';

export default class Collapse extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isActive: props.isActive || false
    };
  }

  static propTypes = {
    isShowIcon: PropTypes.bool,
    disabled: PropTypes.bool,
    style: PropTypes.object
  };

  static defaultProps = {
    isShowIcon: false,
    disabled: false,
    style: {}
  };

  _toggleCollapse = event => {
    if (!this.props.isActive) {
      if (event) {
        event.preventDefault();
      }

      this.setState({ isActive: !this.state.isActive });
    }
  };

  render() {
    const {
      className,
      label,
      container,
      isShowIcon,
      disabled,
      style
    } = this.props;
    const { isActive } = this.state;

    return (
      <div className={`collapse${className ? ' ' + className : ''}`}>
        <div
          className={`collapse-label${isActive ? ' show' : ' hide'}`}
          onClick={e => {
            if (!isShowIcon && !disabled) {
              // NOTE: support cho case ko dùng icon mà click vào Label để collapse/expand
              this._toggleCollapse(e);
            }
          }}
        >
          <div className={`label-wrapper${isShowIcon ? '' : ' static'}`}>
            {label}
          </div>

          {isShowIcon && (
            <Button iconName="arrow" onClick={this._toggleCollapse} />
          )}
        </div>

        <div
          className={`collapse-container${isActive ? ' show' : ' hide'}`}
          style={style}
        >
          {container}
        </div>
      </div>
    );
  }
}

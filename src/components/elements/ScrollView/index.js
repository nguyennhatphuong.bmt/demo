/*
 * Desc: Scrollable view with customize scrollbar
 */

import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import PerfectScrollbar from 'perfect-scrollbar';
import classNames from 'classnames';

// Style
import './style.scss';

export default class ScrollView extends PureComponent {
  static propsType = {
    className: PropTypes.string,
    style: PropTypes.object,
    height: PropTypes.oneOfType([
      PropTypes.number,
      PropTypes.string,
      PropTypes.func
    ]),
    startThreshold: PropTypes.number,
    endThreshold: PropTypes.number,
    onStartReach: PropTypes.func,
    onEndReach: PropTypes.func
  };

  static defaultProps = {
    className: null,
    style: {},
    height: null,
    startThreshold: 10,
    endThreshold: 10
  };

  constructor(props) {
    super(props);
    this._calledEnd = false;
    this._calledStart = false;
  }

  componentDidMount() {
    this._ps = new PerfectScrollbar(this._wrapper);
    if (this._wrapper) {
      this._wrapper.addEventListener('ps-scroll-y', this._handleScroll);
      this._wrapper.addEventListener('ps-scroll-up', this._handleScrollUp);
      this._wrapper.addEventListener('ps-scroll-down', this._handleScrollDown);
      this._wrapper.addEventListener('ps-y-reach-start', this._handleScrollUp);
      this._wrapper.addEventListener('ps-y-reach-end', this._handleScrollDown);
      this._wrapper.addEventListener(
        'DOMSubtreeModified',
        this._handleContentChange
      );
    }
  }

  componentWillUnmount() {
    if (this._wrapper) {
      this._wrapper.removeEventListener('ps-scroll-y', this._handleScroll);
      this._wrapper.removeEventListener('ps-scroll-up', this._handleScrollUp);
      this._wrapper.removeEventListener(
        'ps-scroll-down',
        this._handleScrollDown
      );
      this._wrapper.removeEventListener(
        'ps-y-reach-start',
        this._handleScrollUp
      );
      this._wrapper.removeEventListener(
        'ps-y-reach-end',
        this._handleScrollDown
      );
      this._wrapper.removeEventListener(
        'DOMSubtreeModified',
        this._handleContentChange
      );
    }
    if (this._ps) {
      this._ps.destroy();
    }
  }

  update = () => {
    this._ps.update();
  };
  showScrollbar = () => {
    this._ps.lastScrollTop = -1;
    this._ps.onScroll();
  };

  scrollToTop = () => {
    if (this._ps) {
      if (this._ps.element) {
        this._ps.element.scrollTop = 0;
        this._ps.update();
      }
    }
  };

  _handleScroll = () => {
    const lastScrollTop = this._ps.lastScrollTop;
    const containerHeight = this._ps.containerHeight;
    const contentHeight = this._ps.contentHeight;
    const scrollRest = contentHeight - (lastScrollTop + containerHeight);
    this._calledEnd = this.props.endThreshold < scrollRest;
    this._calledStart = lastScrollTop >= this.props.startThreshold;
    if (typeof this.props.onScroll === 'function') {
      this.props.onScroll();
    }
  };

  _handleContentChange = () => {
    this._calledEnd = false;
    this._calledStart = false;
  };

  _handleScrollUp = () => {
    const lastScrollTop = this._ps.lastScrollTop;

    if (
      !this._calledStart &&
      lastScrollTop < this.props.startThreshold &&
      typeof this.props.onStartReach === 'function'
    ) {
      this._calledStart = true;
      this.props.onStartReach();
    }
  };

  _handleScrollDown = () => {
    const lastScrollTop = this._ps.lastScrollTop;
    const containerHeight = this._ps.containerHeight;
    const contentHeight = this._ps.contentHeight;
    const scrollRest = contentHeight - (lastScrollTop + containerHeight);

    if (
      !this._calledEnd &&
      scrollRest < this.props.endThreshold &&
      typeof this.props.onEndReach === 'function'
    ) {
      this._calledEnd = true;
      this.props.onEndReach();
    }
  };

  render() {
    const { children, style, id } = this.props;
    let { className, height } = this.props;

    className = classNames('e-scrollview', className, {
      [` ${className}`]: className === true
    });

    // NOTE: default value sẽ control bằng CSS
    if (height) {
      height = {
        maxHeight: typeof height === 'string' ? height : `${height}rem`
      };
    }

    let scrollViewStyle = { ...style, ...height };

    return (
      <div
        className={className}
        id={id}
        style={scrollViewStyle}
        ref={r => {
          this._wrapper = r;
        }}
      >
        <div className="e-scrollview-content">{children}</div>
      </div>
    );
  }
}

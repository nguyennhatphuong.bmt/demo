/*
 * Desc: Popup
 * */

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

// Style
import './style.scss';

export default class Popup extends Component {
  static propTypes = {
    isOpen: PropTypes.bool,
    className: PropTypes.string,
    content: PropTypes.object
  };

  static defaultProps = {
    isOpen: false,
    className: '',
    content: null
  };

  wrapperOutsideRef = null;

  componentDidMount() {
    if (this.props.isOpen) {
      document.body.classList.remove('popup-opening');
      document.body.classList.add('popup-opening');
      document.addEventListener('mousedown', this.handleClickOutside);
    } else {
      document.body.classList.remove('popup-opening');
      document.removeEventListener('mousedown', this.handleClickOutside);
    }
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.isOpen !== nextProps.isOpen) {
      if (nextProps.isOpen) {
        document.body.classList.remove('popup-opening');
        document.body.classList.add('popup-opening');
        document.addEventListener('mousedown', this.handleClickOutside);
      } else {
        document.body.classList.remove('popup-opening');
        document.removeEventListener('mousedown', this.handleClickOutside);
      }
    }
  }

  handleClickOutside = event => {
    if (
      this.props.isOpen &&
      this.wrapperOutsideRef &&
      !this.wrapperOutsideRef.contains(event.target) &&
      typeof this.props.clickOutside === 'function'
    ) {
      this.props.clickOutside();
    }
  };

  render() {
    const { isOpen, content, children } = this.props;
    let { className } = this.props;

    className = classNames('popup-overlay', className, {
      [className]: className === true,
      open: isOpen === true
    });

    return (
      <div className={className}>
        <div
          className="popup-container"
          ref={c => {
            this.wrapperOutsideRef = c;
          }}
        >
          <div className="content-wrapper">
            {/* TODO: wrapper class bên trong nên thống nhất là .popup-content */}
            {children || content}
          </div>
        </div>
      </div>
    );
  }
}

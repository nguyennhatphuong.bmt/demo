import React, { Component } from 'react';
import ImageGallery from 'react-image-gallery';

// Styles
import './style.scss';

export default class Slider extends Component {
  constructor(props) {
    super(props);

    this.state = {
      arrayImages: this.props.images,
      status: false
    };

    this._Slider = null;
  }

  // _onSlide = index => {
  //   console.log('test', this._Slider.getCurrentIndex());
  // };

  _renderCustom = () => {
    const { menu } = this.props;

    let menuContent = null;
    if (typeof menu === 'function') {
      menuContent = this.props.menu();
    }

    return menuContent;
  };

  render() {
    const { index } = this.state;

    return (
      <ImageGallery
        ref={ref => {
          this._Slider = ref;
        }}
        showFullscreenButton={false}
        showPlayButton={false}
        autoPlay={this.state.status}
        showBullets={this.props.showBullets}
        showThumbnails={this.props.showThumbnails}
        thumbnailPosition={this.props.thumbnailPosition}
        renderLeftNav={this.props.renderLeftNav}
        renderRightNav={this.props.renderRightNav}
        items={this.state.arrayImages}
        renderCustomControls={() => this._renderCustom()}
        // onSlide={this._onSlide}
        // onClick={this._onClick}
      />
    );
  }
}

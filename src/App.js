import React, { Component } from 'react';
import { Switch, Route, withRouter } from 'react-router-dom';
import routesConfig from './routers/routesConfig';

class App extends Component {
  render() {
    return (
      <Switch>
        {Object.keys(routesConfig).map(pathName => (
          <Route exact {...routesConfig[pathName]} key={`route-${pathName}`} />
        ))}
      </Switch>
    );
  }
}

export default withRouter(App);

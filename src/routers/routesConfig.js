import React from 'react';
import { Redirect } from 'react-router-dom';

// Containers
import { ComingSoonPage, DemoPage, SharePage, NotFoundPage } from '../pages';

this.routes = {
  comingsoon: { path: '/coming-soon', component: ComingSoonPage },
  demo: { path: '/demo', component: DemoPage },
  share: { path: '/share', component: SharePage },
  others: { component: () => <Redirect to="/demo" /> }
};

export default this.routes;

module.exports = {
  extends: 'airbnb',
  parser: 'babel-eslint',
  plugins: ['react', 'jsx-a11y', 'import'],
  rules: {
    quotes: [1, 'single'],
    'react/jsx-filename-extension': [1, { extensions: ['.js', '.jsx'] }],
    'react/prefer-stateless-function': 0,
    'react/jsx-no-bind': 'warn',
    'jsx-a11y/label-has-for': 0,
    'function-paren-newline': 'warn',
    'react/prop-types': 'warn',
    'no-unused-vars': 0,
    'no-else-return': 'warn',
    'react/forbid-prop-types': 'warn',
    'no-unused-expressions': 'warn',
    'react/default-props-match-prop-types': 'warn',

    'import/prefer-default-export': 'warn',
    'space-in-parens': 'warn',
    'react/jsx-curly-brace-presence': 0,
    'import/no-extraneous-dependencies': 'warn',

    'comma-dangle': 'warn',
    'import/no-duplicates': 'warn',
    'react/no-unused-prop-types': 0,
    'import/first': 'warn',

    'react/jsx-no-target-blank': 0,

    // Error when using linter on version 2.0
    'react/jsx-indent': 'warn',
    'no-tabs': 'warn',
    'arrow-body-style': 0,
    indent: 'warn',
    semi: 'warn',
    'react/jsx-indent-props': 'warn',
    'padded-blocks': 'warn',
    'prefer-template': 'warn',
    'spaced-comment': 'warn',
    'no-trailing-spaces': 'warn',
    'no-mixed-spaces-and-tabs': 'warn',
    'react/jsx-closing-tag-location': 'warn',
    'eol-last': 'warn',
    'react/jsx-closing-bracket-location': 'warn',
    'linebreak-style': 0,
    'no-underscore-dangle': 0,
    'react/forbid-prop-types': 0,

    // react hiện tại cần fix lại theo style guide airbnb
    'react/sort-comp': 0,
    'object-shorthand': 0,
    'arrow-parens': 0,
    'prefer-destructuring': 0,
    'func-names': 0,
    'import/extensions': 0,
    'prefer-arrow-callback': 0,
    'no-mixed-operators': 0,
    'no-lonely-if': 0,
    'prefer-const': 0,
    'consistent-return': 'warn',
    'prefer-template': 0,
    'no-param-reassign': 'warn',
    'wrap-iife': 'warn',
    'max-len': 0,
    'no-plusplus': 0,
    'jsx-quotes': ['error', 'prefer-double'],
    'jsx-a11y/anchor-is-valid': 0,
    'import/prefer-default-export': 0,
    'react/prop-types': 0,
    'no-console': 0,
    'import/no-dynamic-require': 0,
    'import/no-extraneous-dependencies': 0,
    'react/no-unused-state': 0,
    'react/no-danger': 0,
    'jsx-a11y/no-static-element-interactions': 0,
    'jsx-a11y/click-events-have-key-events': 0,
    'jsx-a11y/interactive-supports-focus': 0,

    'react/no-did-mount-set-state': 0,
    'no-alert': 0,
    camelcase: 0,
    'function-paren-newline': 0,
    'object-curly-newline': 0,
    'react/no-array-index-key': 0,
    'consistent-return': 0,
    'no-param-reassign': 0,
    'comma-dangle': 0,
    'react/no-multi-comp': 0,
    'jsx-a11y/media-has-caption': 0,
    'no-shadow': 0,
    'no-return-assign': 0,
    'no-case-declarations': 0,
    'space-before-function-paren': 0,
    'jsx-a11y/no-autofocus': 0,
    'operator-linebreak': 0,
    'react/jsx-one-expression-per-line': 0,
    'lines-between-class-members': 0,
    'react/button-has-type': 0,
    'react/no-this-in-sfc': 0,
    'implicit-arrow-linebreak': 0,
    'import/no-useless-path-segments': 0,
    'react/jsx-wrap-multilines': 0,
    'react/jsx-tag-spacing': 0,
    'no-else-return': 0,
    'array-callback-return': 0,
    'no-useless-escape': 0,
    'react/destructuring-assignment': [0, 'never'],
    'react/no-access-state-in-setstate': 0,
    'jsx-a11y/label-has-associated-control': 0,
    // 'react/no-deprecated': 0,
    'prefer-promise-reject-errors': 0,
  },
  env: {
    es6: true,
    node: true,
    browser: true,
  },
  parserOptions: {
    ecmaVersion: 2018,
    sourceType: 'module',
    ecmaFeatures: {
      jsx: true,
      // experimentalObjectRestSpread: true,
      experimentalDecorators: true,
    },
  },
};
